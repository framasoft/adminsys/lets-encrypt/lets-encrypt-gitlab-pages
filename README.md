[![](https://framagit.org/assets/favicon-075eba76312e8421991a0c1f89a89ee81678bcde72319dd3e8047e2a47cd3a42.ico)](https://framagit.org)

![English:](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/20px-Flag_of_the_United_Kingdom.svg.png) **Framasoft uses GitLab** for the development of its free softwares. Our Github repositories are only mirrors.
If you want to work with us, **fork us on [framagit.org](https://framagit.org)**. (no registration needed, you can sign in with your Github account)

![Français :](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/20px-Flag_of_France.svg.png) **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts Github ne sont que des miroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [framagit.org](https://framagit.org)**. (l'inscription n'est pas nécessaire, vous pouvez vous connecter avec votre compte Github)
* * *

# Let's encrypt Gitlab Pages!

This script will create [Let's encrypt](https://letsencrypt.org/) for [Gitlab Pages](https://docs.gitlab.com/ce/user/project/pages/index.html) with custom domains.

## Dependencies

You will need Carton, a Perl dependencies manager, it will get what you need, so don't bother about dependencies (but you can read the file cpanfile if you want).

```
sudo cpanm -q Carton
```

You will need some dev packages on your system. For Debian:
```
sudo apt install build-essential libssl-dev zlib1g-dev
```

You will also need [Certbot](https://certbot.eff.org/), a Let's encrypt client.

See <https://certbot.eff.org/> to install it on your system.

## Installation

```
git clone https://framagit.org/framasoft/lets-encrypt-gitlab-pages /opt/lepages
cd /opt/lepages
carton install --deployment
cp config.json.template config.json
vi config.json
```

## Configuration

There's few options in `config.json`, it's simple:
- `email`: the email to use with `certbot`. It will be used to warn you about stuff like "Your certificate is about to expire". MANDATORY.
- `pages_dir`: the directory containing Gitlab Pages projects. OPTIONAL, default is `/var/opt/gitlab/gitlab-rails/shared/pages/`, which is the Gitlab Pages directory if you installed Gitlab with the omnibus package.
- `certbot`: path to `certbot`. OPTIONAL, default is `/usr/bin/certbot`.
- `gitlab_url`: the address of your Gitlab's v4 API (like `https://gitlab.example.org/api/v4`)
- `secret_token`: a secret token created by someone that has admin access to Gitlab. Create it at https://gitlab.example.org/profile/personal_access_tokens.
- `ips`: a hash table containing:
  - `ipv4`: IPv4 that your Gitlab pages daemon uses
  - `ipv6`: IPv6 that your Gitlab pages daemon uses

## Use

Just launch `/opt/lepages/lepages`: it will create certificates and send them to Gitlab (if needed).

Create a cron task to fully automate the certificate creation.

If a project has already a Let's Encrypt certificate, it will update it if needed.

There is two options:
- `-n|--dry-run`: will make a certbot dry run, not creating real certificates, and will not send any certificate to Gitlab
- `-v|--verbose`: gives some output (lepages won't say a thing per default, except if something goes wrong)


