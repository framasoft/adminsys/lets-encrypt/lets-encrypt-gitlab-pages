requires "Mojolicious";
requires "Email::Valid";
requires "GitLab::API::v4";
requires "IO::Socket::SSL";
requires "Term::SimpleColor";
requires "NetAddr::IP";
requires "Net::DNS";
