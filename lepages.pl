#!/usr/bin/perl
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
use Mojo::Base -base;
use Mojo::JSON qw(encode_json decode_json);
use Mojo::File;
use Mojo::Collection 'c';
use Email::Valid;
use GitLab::API::v4;
use Net::DNS;
use NetAddr::IP;
use Getopt::Long;
use Term::SimpleColor;

###
## Get config
#
my $config = decode_json(Mojo::File->new('config.json')->slurp) || {};

# Apply default config
$config->{certbot}   = '/usr/bin/certbot'                      unless defined $config->{certbot};
$config->{pages_dir} = '/var/opt/gitlab/gitlab-rails/shared/pages/' unless defined $config->{pages_dir};

# Check requirements
die "You have to set Gitlab URL in config.json!"               unless (defined($config->{gitlab_url}));
die "You have to set an API token in config.json!"             unless (defined($config->{secret_token}));
die "You have to set a valid email address in config.json!"    unless (defined($config->{email}) && Email::Valid->address($config->{email}));
die "You have to set IP (v4 and v6) addresses in config.json!" unless (defined($config->{ips}) && $config->{ips}->{ipv4} && $config->{ips}->{ipv6});

###
## Create global vars
#
my $api = GitLab::API::v4->new(
    url           => $config->{gitlab_url},
    private_token => $config->{secret_token},
);

# Create name resolver object
my $dns = Net::DNS::Resolver->new;

# Get IPs from the config
my $ipv4 = NetAddr::IP->new($config->{ips}->{ipv4});
my $ipv6 = NetAddr::IP->new6($config->{ips}->{ipv6});

###
## Get options
#
shift @ARGV if ($ARGV[0] eq '--');
my ($dry_run, $verbose) = (0, 0);
GetOptions(
    'dry-run|d'       => \$dry_run,
    'verbose|v'       => \$verbose
);
say "Dry run" if $dry_run;

###
## Get all domains from Gitlab
#
say cyan 'Get all domains from Gitlab' if $verbose;
my %projects;
my $paginator = $api->paginator('global_pages_domains');

# Group domains by project_id
while (my $dom = $paginator->next()) {
    $projects{$dom->{project_id}} = [] unless defined $projects{$dom->{project_id}};
    push @{$projects{$dom->{project_id}}}, $dom;
}

###
## Process all projects
#
say cyan 'Processing projects' if $verbose;
c(sort keys %projects)->each(sub {
    my ($project_id, $num) = @_;

    # Get project details from Gitlab
    my $p = $api->project($project_id);

    # Get project domains from Gitlab
    say cyan '  Get projects domains for '.$p->{path_with_namespace} if $verbose;
    my $api_domains = $api->pages_domains($p->{path_with_namespace});

    # Process only verified domains
    my @verified_d;
    for my $i (@{$api_domains}) {
        push @verified_d, $i if ($i->{verified} && check_if_dns_ok($i->{domain}));
    }
    my @domains     = map {$_->{domain}} @verified_d;
    my @all_domains = map {$_->{domain}} @{$api_domains};
    say cyan "    All domains of $p->{path_with_namespace}: @all_domains"   if $verbose;
    if (scalar(@domains)) {
        say green "    Verified domains for $p->{path_with_namespace}: @domains" if $verbose;
    } else {
        say red "    [NEXT] **** NO verified domains for $p->{path_with_namespace}! ****" if $verbose;
        return;
    }

    # Per default, don't create certificate
    my $go_certbot  = 0;

    # Ok, we have verified domains
    my @new_domains;
    my @expired;
    for my $i (@verified_d) {
        if ($i->{certificate}) {
            # Domains with expired certificates
            push @expired,     $i->{domain} if $i->{certificate}->{expired};
        } else {
            # New domains: don't have certificate
            push @new_domains, $i->{domain};
        }
    }
    say yellow "    Domains without certificate: @new_domains"  if ($verbose && scalar(@new_domains));
    say yellow "    Domains with expired certificate: @expired" if ($verbose && scalar(@expired));

    # What should be the LE's certificate namespace?
    my $le = $domains[0];
    for my $i (@domains) {
        # We check if one of the domain already has a renewal conf
        # => indicates LE's certificate namespace
        if (-e (Mojo::File->new('/etc/letsencrypt/renewal/', $i.'.conf')->to_string)) {
            $le = $i;
            last;
        }
    }
    say cyan "    LE's certificate namespace: $le" if $verbose;

    # What are the name of LE's files?
    my $key = Mojo::File->new('/etc/letsencrypt/live/', $le, 'privkey.pem');
    my $pem = Mojo::File->new('/etc/letsencrypt/live/', $le, 'fullchain.pem');
    my $lec = Mojo::File->new('/etc/letsencrypt/renewal/', $le.'.conf');

    # Is there a new domain for the project? (expand)
    if (-e $lec) {
        $lec = $lec->slurp;

        for my $i (@new_domains) {
            if ($lec !~ m#$i#m) {
                $go_certbot = 1;
                last;
            }
        }
    } else {
        # Use certbot if there is domains without certificate or expired one
        if (scalar(@new_domains) || scalar(@expired)) {
            $go_certbot = 1;
        } else {
            say green "    [NEXT] No domains without certificate or expired one" if $verbose;
            return;
        }
        # All domains are new to us
        say yellow "    /etc/letsencrypt/renewal/$le.conf does not exist" if $verbose;
    }
    my $dir_exists = -d Mojo::File->new($config->{pages_dir}, $p->{path_with_namespace}, 'public')->to_string;

    # Let's create a certificate (if needed)
    if ($go_certbot && $dir_exists) {
        my @args = (
            $config->{certbot},
            'certonly',
            '--rsa-key-size',
            '4096',
            '--quiet',
            '--webroot',
            '--webroot-path',
            Mojo::File->new($config->{pages_dir}, $p->{path_with_namespace}, 'public')->to_string,
            '--email',
            $config->{email},
            '--agree-tos',
            '--text',
            '--expand',
            '--non-interactive',
            '--domains',
            join(',', @domains)
        );
        push @args, '--dry-run' if $dry_run;

        say cyan "    Starting certbot" if $verbose;
        system(@args) == 0 or die "system @args failed: $!";
    }


    # Well, it's better to check that we can read the certificate and the key
    if ((-r $key && -r $pem && $dir_exists) || $dry_run) {
        # We'll need pem content even if we don't update the domains on Gitlab
        my $pem_content = $pem->slurp unless $dry_run;

        # Logically, running certbot means that we need to update the domains on Gitlab
        my $updated_cert = $go_certbot;

        # What if we didn't ran certbot?
        if (!$updated_cert && $pem_content) {
            for my $i (@verified_d) {
                # Update the domains on Gitlab if one of the certificate is not the same as LE one (a certbot renew for ex.)
                if ($pem_content ne $i->{certificate}->{certificate}) {
                    say yellow "    $i->{domain} has a different certificate than LE" if $verbose;
                    $updated_cert = 1;
                }
            }
        }

        # Let's update the domains on Gitlab
        if ($updated_cert) {
            my $key_content = $key->slurp unless $dry_run;
            for my $i (@domains) {
                if ($dry_run) {
                    say "[DRY RUN] Would have send new certificate for $i ($p->{path_with_namespace})";
                } else {
                    say green "    Sending new certificate for $i ($p->{path_with_namespace})" if $verbose;
                    $api->edit_pages_domain(
                        $p->{path_with_namespace},
                        $i,
                        {
                            certificate => $pem_content,
                            key         => $key_content
                        }
                    );
                }
            }
        } else {
            say green "    [NEXT] No need to send certificate for $p->{path_with_namespace}" if $verbose;
        }
    } elsif ($dir_exists) {
        say "Strange thing, unable to read $key and/or $pem, that should not happen.";
    }
});

sub check_if_dns_ok {
    my $host = shift;

    my $ok     = 1;

    my $reply = $dns->search($host, 'A');
    if ($reply) {
        foreach my $rr ($reply->answer) {
            if ($rr->can("address")) {
                $ok = 0 unless (NetAddr::IP->new($rr->address) == $ipv4);
            } elsif ($rr->can("cname")) {
                $ok = check_if_dns_ok($rr->cname);
            } else {
                $ok = 0;
                last;
            }
        }
    } else {
        $ok = 0;
        warn "A query failed for $host: ", $dns->errorstring, "\n" if $dry_run;
    }

    $reply = $dns->search($host, 'AAAA');
    if ($reply) {
        foreach my $rr ($reply->answer) {
            if ($rr->can("address")) {
                $ok = 0 unless (NetAddr::IP->new6($rr->address) == $ipv6);
            } elsif ($rr->can("cname")) {
                $ok = check_if_dns_ok($rr->cname);
            } else {
                $ok = 0;
                last;
            }
        }
    } else {
        warn "AAAA query failed for $host: ", $dns->errorstring, "\n" if $dry_run;
    }

    return $ok;
}
